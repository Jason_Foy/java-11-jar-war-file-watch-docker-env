export abstract class Thread {

    protected running_process: any;
    protected fs: any;
    protected child_process: any;
    protected filename: string;
    protected path: string;

    /**
     *
     * @param fs
     * @param child_process
     * @param path
     * @param filename
     */
    protected constructor(fs: any, child_process: any, path: string, filename: string) {
        this.fs = fs;
        this.child_process = child_process;
        this.filename = filename;
        this.path = path;
    }

    /**
     *
     * @param callback
     */
    abstract start_thread(callback: Function): void;

    /**
     * @return string
     */
    get_filename(): string {
        return this.filename;
    }
}
