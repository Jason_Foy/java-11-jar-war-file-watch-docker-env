import {Thread} from './thread';


/**
 * Manage a Single Thread Instance
 */
export default class JavaFileWatcher extends Thread {

    type: string;

    /**
     *
     * @param fs
     * @param child_process
     * @param path
     * @param filename
     * @param type
     */
    constructor(fs: any, child_process: any, path: string, filename: string, type: string) {
        super(fs, child_process, path, filename);
        this.type = type.toLowerCase();
    }

    /**
     *
     * @param callback
     */
    private start_jar_thread(callback: Function): void {
        let callback_called = false;
        this.running_process = this.child_process.exec('java -jar ' + this.path + this.filename + '&',
            function (error: any, out: any, stdin: any) {
                if (error)
                    throw error;
            });

        this.running_process.stdout.on('data', function (chunk: any) {

            if (!callback_called) {
                callback_called = true;
                callback(true);
            }
            console.log(chunk)
        });
    }

    /**
     *
     * @param callback
     */
    private start_tomcat_thread(callback: Function): void {
        console.log('copy file', this.path + this.filename);
        /** Copy War File **/
        this.fs.copyFile(this.path + this.filename, '/srv/tomcat/webapps/' + this.filename, (err: any) => {
            if (err) throw err;

            /** Start TomCat **/
            this.running_process = this.child_process.exec('/srv/tomcat/bin/catalina.sh start',
                function (err: any, stdout: any, stderr: any) {
                    if (err)
                        callback(false, err);
                    else
                        callback(true, null)
                });
        });
    }

    /**
     * @param callback Function
     */
    start_thread(callback: Function = (result: boolean) => {
        return result;
    }): void {
        switch (this.type) {
            case 'jar':
                this.start_jar_thread(callback);
                break;
            case 'war':
                this.start_tomcat_thread(callback);
                break;
        }
    }

    protected kill_running_process(callback: Function): boolean {
        if (this.type === 'war')
            this.child_process.exec('/srv/tomcat/bin/catalina.sh stop');
        else {
            let test = this.child_process.exec("kill $(ps aux | grep \"java\" | grep -v 'grep' | awk '{print $2}')", function () {
                callback();
            });
        }
        return true;
    }

    /**
     * @return boolean
     */
    start_file_watch(callback: Function = (result: boolean) => {
        return result;
    }): void {
        /** Watch File Binding**/
        this.fs.watchFile(this.path + this.get_filename(), {interval: 1000}, (curr: any, prev: any) => {
            /** Kill Existing Process **/
            this.kill_running_process(() => {
                this.start_thread(callback);
            });
        });
    }
}