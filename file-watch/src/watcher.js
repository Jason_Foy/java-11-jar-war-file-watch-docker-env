const fs = require('fs');
import JavaFileWatcher from './javaFileWatcher';

const config = {
    path: '/application/target/',
    filename: process.argv[2],
    extension: process.argv[2].split('.').pop(),
    fullPath: () => {
        return config.path + config.filename
    }
};

const jfw = new JavaFileWatcher(
    fs,
    require('child_process'),
    config.path,
    config.filename,
    config.extension
);


/** Check if Target file exists **/
try {
    fs.existsSync(config.path);
    fs.existsSync(config.fullPath());
} catch (err) {
    console.log('Unable to find file:' + jfw.getFilename())
}

jfw.start_thread((result) => {
    console.log('Init Thread Started', result);

    if (result) {
        console.log('File Watch Service Started.');
        jfw.start_file_watch((result) => {
            console.log('New Thread Hosted');

        });
    }

});