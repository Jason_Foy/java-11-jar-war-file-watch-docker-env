"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Thread = /** @class */ (function () {
    /**
     *
     * @param fs
     * @param child_process
     * @param path
     * @param filename
     */
    function Thread(fs, child_process, path, filename) {
        this.fs = fs;
        this.child_process = child_process;
        this.filename = filename;
        this.path = path;
    }
    /**
     * @return string
     */
    Thread.prototype.get_filename = function () {
        return this.filename;
    };
    return Thread;
}());
exports.Thread = Thread;
