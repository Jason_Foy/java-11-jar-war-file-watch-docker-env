"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require('fs');
var javaFileWatcher_1 = __importDefault(require("./javaFileWatcher"));
var config = {
    path: '/application/target/',
    filename: process.argv[2],
    extension: process.argv[2].split('.').pop(),
    fullPath: function () {
        return config.path + config.filename;
    }
};
var jfw = new javaFileWatcher_1.default(fs, require('child_process'), config.path, config.filename, config.extension);
/** Check if Target file exists **/
try {
    fs.existsSync(config.path);
    fs.existsSync(config.fullPath());
}
catch (err) {
    console.log('Unable to find file:' + jfw.getFilename());
}
jfw.start_thread(function (result) {
    console.log('Init Thread Started', result);
    if (result) {
        console.log('File Watch Service Started.');
        jfw.start_file_watch(function (result) {
            console.log('New Thread Hosted');
        });
    }
});
