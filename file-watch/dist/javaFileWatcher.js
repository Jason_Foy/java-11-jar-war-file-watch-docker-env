"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var thread_1 = require("./thread");
/**
 * Manage a Single Thread Instance
 */
var JavaFileWatcher = /** @class */ (function (_super) {
    __extends(JavaFileWatcher, _super);
    /**
     *
     * @param fs
     * @param child_process
     * @param path
     * @param filename
     * @param type
     */
    function JavaFileWatcher(fs, child_process, path, filename, type) {
        var _this = _super.call(this, fs, child_process, path, filename) || this;
        _this.type = type.toLowerCase();
        return _this;
    }
    /**
     *
     * @param callback
     */
    JavaFileWatcher.prototype.start_jar_thread = function (callback) {
        var callback_called = false;
        this.running_process = this.child_process.exec('java -jar ' + this.path + this.filename + '&', function (error, out, stdin) {
            if (error)
                throw error;
        });
        this.running_process.stdout.on('data', function (chunk) {
            if (!callback_called) {
                callback_called = true;
                callback(true);
            }
            console.log(chunk);
        });
    };
    /**
     *
     * @param callback
     */
    JavaFileWatcher.prototype.start_tomcat_thread = function (callback) {
        var _this = this;
        console.log('copy file', this.path + this.filename);
        /** Copy War File **/
        this.fs.copyFile(this.path + this.filename, '/srv/tomcat/webapps/' + this.filename, function (err) {
            if (err)
                throw err;
            /** Start TomCat **/
            _this.running_process = _this.child_process.exec('/srv/tomcat/bin/catalina.sh start', function (err, stdout, stderr) {
                if (err)
                    callback(false, err);
                else
                    callback(true, null);
            });
        });
    };
    /**
     * @param callback Function
     */
    JavaFileWatcher.prototype.start_thread = function (callback) {
        if (callback === void 0) { callback = function (result) {
            return result;
        }; }
        switch (this.type) {
            case 'jar':
                this.start_jar_thread(callback);
                break;
            case 'war':
                this.start_tomcat_thread(callback);
                break;
        }
    };
    JavaFileWatcher.prototype.kill_running_process = function (callback) {
        if (this.type === 'war')
            this.child_process.exec('/srv/tomcat/bin/catalina.sh stop');
        else {
            var test = this.child_process.exec("kill $(ps aux | grep \"java\" | grep -v 'grep' | awk '{print $2}')", function () {
                callback();
            });
        }
        return true;
    };
    /**
     * @return boolean
     */
    JavaFileWatcher.prototype.start_file_watch = function (callback) {
        var _this = this;
        if (callback === void 0) { callback = function (result) {
            return result;
        }; }
        /** Watch File Binding**/
        this.fs.watchFile(this.path + this.get_filename(), { interval: 1000 }, function (curr, prev) {
            /** Kill Existing Process **/
            _this.kill_running_process(function () {
                _this.start_thread(callback);
            });
        });
    };
    return JavaFileWatcher;
}(thread_1.Thread));
exports.default = JavaFileWatcher;
